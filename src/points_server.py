#Francesca 'Jade' Truncale, 2021
#API Server, provides requested routes for adding, deducting, and getting point balance
import asyncio
import signal
import logging
import sys

from datetime import datetime

from aiohttp import web

from models import Transaction, User, ConvertedDatetime

class PointsServer():
    
    def __init__(self, ws_hostname='localhost', ws_port=3000):

        self._ws = None
        self._wsRunner = None

        self._wsHostname = ws_hostname
        self._wsPort = ws_port

        self._log = logging.getLogger("{name}".format(name=__name__))
        self._setupLogger()

        self._users = {} #Temporary storage for users

        #Create event loop for server
        self._event_loop = asyncio.get_event_loop()
        asyncio.set_event_loop(self._event_loop)

        self._wsInit()

        #Start server loop
        self._init()

    def _setupLogger(self):
        self._log.setLevel(logging.DEBUG)
        handler = logging.StreamHandler(sys.stdout)
        handler.setLevel(logging.DEBUG)
        formatter=logging.Formatter('[%(levelname)s] %(asctime)s - %(message)s')
        handler.setFormatter(formatter)
        self._log.addHandler(handler)

    def _setupSignalHandlers(self):
        signals = (signal.SIGHUP, signal.SIGTERM, signal.SIGINT)
        for s in signals:
            self._event_loop.add_signal_handler(s, lambda  s=s: self._event_loop.create_task(self.shutdown()))

    def _init(self):
        self._setupSignalHandlers()

        try:
            self._log.info("{name}) now starting on {host}:{port}...".format(name=__name__,
                                                                                host=self._wsHostname,
                                                                                port=self._wsPort))    
            self._event_loop.create_task(self._ws.start())
            self._event_loop.run_forever()

        finally:
            pass

    def _wsInit(self):
        app = web.Application()

        # Setup routes
        app.router.add_post('/addUser', self._addUser)
        app.router.add_post('/addPoints', self._addPoints)
        app.router.add_post('/deductPoints', self._deductPoints)
        app.router.add_get('/pointsBalance', self._getPoints)

        self._wsRunner = web.AppRunner(app)
        self._event_loop.run_until_complete(self._wsRunner.setup())
        self._ws = web.TCPSite(self._wsRunner, self._wsHostname, self._wsPort)

    async def _addPoints(self, request):
        request_body = await request.post()
        user = request_body.get('user')
        payer = request_body.get('payer','')
        points = int(request_body.get('points',0))
        date = request_body.get('date','')

        #If not sent expected user or payer, return bad request
        if ((user not in self._users) or (payer == '') or (points==0)):
            return web.Response(text="Missing parameters", status=400)
        
        else:
            new_points = self._users[user].addPoints(Transaction(payer, points, date))
            response = {payer:new_points}
            return web.json_response(response, status=200)

    async def _deductPoints(self, request):
        request_body = await request.post()
        user = request_body.get('user','')
        points = int(request_body.get('points',0))

        #If not sent expected user or payer, return bad request
        if ((user not in self._users) or (points == '')):
            return web.Response(text="Missing parameters", status=400)

        else:
            #Expects [PAYER, points, now]
            response = self._users[user].deductPoints(points)
            response = list(map(list, response.items()))
            response = [[a, b, ConvertedDatetime().date] for a, b in response] 
            return web.json_response(response, status=200)

    async def _getPoints(self, request):
        user = request.rel_url.query['user']
        response = self._users[user].getPayers()

        #If not sent expected user, return bad request
        if (user == ''):
            return web.Response(status=400) 

        else:
            return web.json_response(response, status=200)

    async def _addUser(self, request):
        request_body = await request.post()
        name = request_body.get('user','')

        #If not sent name for user, return bad request
        if (name == ''):
            return web.Response(body="Missing parameters", status=400)

        #If user already exists
        elif (name in self._users):
            return web.Response(body="User exists", status=204)

        else:
            self._users.update({name:User(name=name)})
            response = {"Created":name}
            return web.json_response(response, status=201)


    async def shutdown(self):
        self._log.info("Shutting Down {name}".format(name=__name__))
        tasks = [t for t in asyncio.all_tasks() if t is not
                 asyncio.current_task()]
        [task.cancel() for task in tasks]
        await asyncio.gather(*tasks, return_exceptions=True)
        await self._wsRunner.cleanup()

        self._event_loop.stop()

        self._log.info("{name} successfully shutdown.".format(name=__name__))