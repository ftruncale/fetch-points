#Francesca 'Jade' Truncale, 2021
#Provides the user model as defined & helper transaction model

from datetime import datetime
from queue import deque 

class ConvertedDatetime():
    def __init__(self, date = ''):
        if date == '':
            date = datetime.now()
        
        self.date = date.strftime("%m/%d %-I%p")
        
class Transaction():
    def __init__(self, payer = "", points = 0, date = ''):
        self.payer = payer
        self.points = points
        self.date = ConvertedDatetime(date).date
        self.remaining = self.points #Hold "remaining" value of this transaction

class User():

    def __init__(self, name = "", balance = 0):
        self.name = name
        self.balance = balance
        self._transactions = deque()
        self._emptied_transactions = [] #Move transactions here to keep track of them, but know they have nothing remaining
        #Stores payers and total points from each
        self._payer_dict = {}

    def getPayers(self):
        return self._payer_dict

    #For adding a negative value to transactions
    def _processSubtract(self, transaction):
        # index = self._transactions.index(0 ,len(self._transactions)-1)
        temp = transaction.points

        for item in self._transactions:
            if item.payer == transaction.payer:
                #If there are enough/more than enough points
                if ((temp + item.remaining) >= 0):
                    item.remaining += temp
                    break

                else:
                    temp += item.remaining
                    item.remaining = 0
                
        self._payer_dict[transaction.payer] += transaction.points

        #New transactions can remove points, check if now below 0.
        if self._payer_dict[transaction.payer] < 0:
            self._payer_dict[transaction.payer] = 0

    def addPoints(self, transaction = Transaction()):
        #checks if the payer_dict already has an instance of the payer
        if transaction.payer not in self._payer_dict:
            self._payer_dict.update({transaction.payer:0})

        if (transaction.points > 0):
            self._payer_dict[transaction.payer] += transaction.points
            self._transactions.append(transaction)

        else:
            self._processSubtract(transaction)
        
        self.balance += transaction.points

        return self._payer_dict[transaction.payer]

    #Returns a dict of payers and deductions
    def deductPoints(self, amount):
        #If user can't afford to spend points, abort
        if ( (self.balance - amount) < 0 ):
            return {}
        
        agg_points = amount
        agg_payer_dict = {}
        
        while agg_points > 0:
            curr_trans = self._transactions[0]
            if curr_trans.payer not in agg_payer_dict:
                agg_payer_dict.update({curr_trans.payer:0})

            temp = curr_trans.remaining - agg_points

            #If we have more points than we need
            if (temp >= 0):
                #Commit change to item
                self._transactions[0].remaining = temp

                agg_payer_dict[curr_trans.payer] -= agg_points
                agg_points = 0 #we no longer need to look for points

                #Store emptied transaction, remove from main transactions
                if (temp == 0):
                    self._emptied_transactions.append(self._transactions.popleft())

            
            #If there aren't enough points in the current transaction
            else:
                agg_points -= curr_trans.remaining
                agg_payer_dict[curr_trans.payer] -= curr_trans.remaining
                #Commit change, then remove from main transactions
                self._transactions[0].remaining = 0
                self._emptied_transactions.append(self._transactions.popleft())
            

        #end while agg_points > 0

        #Remove points from payer_dict as per payer
        for key in agg_payer_dict:
            self._payer_dict[key] += agg_payer_dict[key]
        
            if self._payer_dict[key] < 0:
                self._payer_dict[key] = 0
 
        #Now subtract from the user balance
        self.balance -= amount
        return agg_payer_dict