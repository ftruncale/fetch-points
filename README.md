# Fetch Rewards Coding Excercise

Based on the [points exercise](points.pdf)

##  Quickstart

Requires Python 3. (Used against 3.9.1)

Also, package ``aiohttp``, which can be installed using:

``pip install aiohttp``

Run ``python src/main.py`` to start the server, stop with CTRL+C (it will be caught).

By default, will run on ``localhost:3000``

## Routes
Provided routes include:

* POST ``/addUser``
    - Expects a body of ``{user:[string]}``
    - Creates user for use with other routes

* POST ``/addPoints``
    -  Expects a body of ``{user:[string], payer:[string], points:[integer], date:[date]}``
    -  Returns ``{payer:[total points]}``

* POST ``/deductPoints``
    - Expects a body of ``{user:[string], points:[integer]}``
    - Returns list of ``[payer, deducted amount, time(now)]``

* GET ``/pointsBalance``
    - Expects a body of ``{user:[string]}``
    - Returns payers and their point balances for the given user ``{ payer1:points1, payer2:points2,...}``

### End notes
By [Jade Truncale](https://ftruncale.bitbucket.io/)